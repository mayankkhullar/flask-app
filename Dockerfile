FROM python:3.8
WORKDIR /code
COPY requirements.txt .
EXPOSE 5000
RUN apt-get update ;apt-get -y install libsasl2-dev python-dev libldap2-dev libssl-dev;pip install -r requirements.txt
COPY src/ .
CMD flask run --host=0.0.0.0 --port=5000

