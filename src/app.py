import os
import logging
import boto3
from flask_simpleldap import LDAP
from botocore.exceptions import ClientError
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
from progress import ProgressPercentage


UPLOAD_FOLDER = "/home"

app = Flask(__name__,template_folder='template',static_folder='static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
""" AWS CREDS """
ACCESS_KEY = os.environ['ACCESS_KEY']
SECRET_KEY = os.environ['SECRET_KEY']

""" LDAP PROPERTIES """
app.config['LDAP_HOST'] = '172.17.0.2'  # defaults to localhost
app.config['LDAP_PORT'] = '389'  # defaults to localhost
app.config['LDAP_BASE_DN'] = 'dc=example,dc=org'
app.config['LDAP_USERNAME'] = 'CN=admin,DC=example,DC=org'
app.config['LDAP_PASSWORD'] = 'admin'
app.config['LDAP_USER_OBJECT_FILTER'] = '(&(objectclass=simpleSecurityObject)(cn=%s))'
app.config['LDAP_OPENLDAP'] = True
ldap = LDAP(app)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower()

@app.route('/', methods=['GET', 'POST'])
@ldap.basic_auth_required
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            object_name = filename
            print(os.path.join(app.config['UPLOAD_FOLDER'],filename))
            s3_client = boto3.client('s3',aws_access_key_id=ACCESS_KEY,aws_secret_access_key=SECRET_KEY)
            response = s3_client.upload_file(os.path.join(app.config['UPLOAD_FOLDER'], filename),'my-bucket-711693673091',
                    object_name,Callback=ProgressPercentage(os.path.join(app.config['UPLOAD_FOLDER'], filename)))
    return render_template('index.html')

@app.errorhandler(500)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return "Erro , either creds are not working or bucket does not exist"

if __name__ == '__main__':
    app.run(host='0.0.0.0')
